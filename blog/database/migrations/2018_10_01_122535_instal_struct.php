<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstalStruct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional',function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('price')->nullable();
            $table->timestamps();
        });

        Schema::create('clients',function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->timestamps();
        });

        Schema::create('colours',function (Blueprint $table){
            $table->increments('id');
            $table->enum('name',['Красный','Синий', 'Черный', 'Зеленый'])->default('Черный');
            $table->timestamps();
        });

        Schema::create('comments',function (Blueprint $table)
        {
            $table->increments('id');

            $table->integer('product_id')->unsigned()->default(1);
            $table->foreign('product_id')->references('id')->on('products');

            $table->string('name');
            $table->string('email');
            $table->longText('msg');
            $table->enum('approve',['true','false'])->default('false');
            $table->timestamps();
        });

        Schema::create('galleries',function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('filename');
            $table->integer('sort_id');
            $table->timestamps();
        });

        Schema::create('info',function (Blueprint $table)
        {
            $table->increments('id');
            $table->longText('text');
            $table->timestamps();
        });

        Schema::create('jobs',function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('queue');
            $table->longText('payload');
            $table->tinyInteger('attempts')->unsigned();
            $table->tinyInteger('reserved')->unsigned();
            $table->unsignedInteger('reserved_at')->nullable();
            $table->unsignedInteger('available_at');
            $table->unsignedInteger('created_at');
            $table->index(['queue','reserved','reserved_at']);
        });

        Schema::create('my_categories',function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('cover')->nullable();
            $table->string('icon')->nullable();
            $table->string('urlhash');//add index
            $table->integer('parent_id');
            $table->integer('sort_id');
            $table->timestamps();
        });

        Schema::create('my_password_resets',function (Blueprint $table)
        {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamps();
        });

        Schema::create('my_users',function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password',60);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('npcity',function (Blueprint $table)
        {
            $table->string('name');
            $table->string('ref');
        });

        Schema::create('npunit',function (Blueprint $table)
        {
            $table->string('name');
            $table->string('ref');
        });

        Schema::create('options',function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('price')->nullable();
            $table->timestamps();
        });

        Schema::create('order_add',function (Blueprint $table)
        {
            $table->increments('id');

            $table->integer('order_id')->unsigned()->default(1);
            $table->foreign('order_id')->references('id')->on('orders');

            $table->integer('additional_id')->unsigned()->default(1);
            $table->foreign('additional_id')->references('id')->on('additional');

            $table->timestamps();
        });

        Schema::create('order_files',function (Blueprint $table)
        {
            $table->increments('id');

            $table->integer('order_id')->unsigned()->default(1);
            $table->foreign('order_id')->references('id')->on('orders');

            $table->string('name')->nullable();
            $table->string('hash')->nullable();
            $table->string('mime')->nullable();
            $table->string('extension')->nullable();
            $table->enum('status',['tmp','success'])->default('tmp');
            $table->enum('image',['true','false'])->default('false');
            $table->timestamps();
        });

        Schema::create('order_items',function (Blueprint $table)
        {
            $table->increments('id');

            $table->integer('order_id')->unsigned()->default(1);
            $table->foreign('order_id')->references('id')->on('orders');

            $table->integer('product_id')->unsigned()->default(1);
            $table->foreign('product_id')->references('id')->on('products');


            $table->integer('qty');
            $table->timestamps();
        });

        Schema::create('orders',function (Blueprint $table)
        {
            $table->increments('id');

            $table->integer('client_id')->unsigned()->default(1);
            $table->foreign('client_id')->references('id')->on('clients');

            $table->string('delivery_city');
            $table->string('delivery_adr')->nullable();
            $table->string('delivery_np')->nullable();
            $table->enum('delivery_type',['adr','np'])->default('np');
            $table->enum('pay_type',['null','privat24','privat_terminal','liqpay'])->default('privat24');
            $table->string('code');
            $table->string('ttn')->nullable();
            $table->longText('comment')->nullable();
            $table->enum('status',['new','paid','sent'])->default('new');
            $table->timestamps();

        });

        Schema::create('product_options',function (Blueprint $table)
        {
            $table->increments('id');

            $table->integer('product_id')->unsigned()->default(1);
            $table->foreign('product_id')->references('id')->on('products');

            $table->integer('option_id')->unsigned()->default(1);
            $table->foreign('option_id')->references('id')->on('options');

            $table->timestamps();
        });

        Schema::create('products',function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('title');
            $table->string('keywords');
            $table->longText('description');
            $table->longText('description_full')->nullable();
            $table->string('values')->nullable();
            $table->string('cover')->nullable();
            $table->string('sku')->nullable();
            $table->string('price')->nullable();
            $table->string('price_old')->nullable();
            $table->string('label')->nullable();
            $table->enum('isset',['true','false'])->default('true');
            $table->enum('visible',['true','false'])->default('true');
            $table->string('urlhash');//add index
            $table->integer('sort_id');

            $table->integer('categories_id')->unsigned()->default(1);
            $table->foreign('categories_id')->references('id')->on('my_categories');

            $table->integer('coloures_id')->unsigned()->default(1);
            $table->foreign('coloures_id')->references('id')->on('colours');

            $table->timestamps();
        });

        Schema::create('recommendsProducts',function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('product_id_recommend');
            $table->timestamps();
        });

        Schema::create('visitor_registry',function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('ip',32);
            $table->string('country',4)->nullable();
            $table->integer('clicks')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('additional');
        Schema::dropIfExists('clients`');
        Schema::dropIfExists('colours`');
        Schema::dropIfExists('comments');
        Schema::dropIfExists('galleries');
        Schema::dropIfExists('info');
        Schema::dropIfExists('jobs');
        Schema::dropIfExists('my_categories');
        Schema::dropIfExists('my_password_resets');
        Schema::dropIfExists('my_users');
        Schema::dropIfExists('npcity');
        Schema::dropIfExists('npunit');
        Schema::dropIfExists('options');
        Schema::dropIfExists('order_add');
        Schema::dropIfExists('order_files');
        Schema::dropIfExists('order_items');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('product_options');
        Schema::dropIfExists('products');
        Schema::dropIfExists('recommendsProducts');
        Schema::dropIfExists('visitor_registry');
    }
}

