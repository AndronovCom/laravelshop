<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NPUnit extends Model
{
    protected $table='npunit';
    protected $fillable=['name','ref'];

    public $timestamps=false;
}
