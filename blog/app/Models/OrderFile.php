<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderFile extends Model
{
   protected  $table='order_files';
   protected  $fillable=['order_id','name','hash','mime','extension','status','image'];
   public  $timestamps=true;

}
