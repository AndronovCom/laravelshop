<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table='galleries';
    protected  $fillable=['filename','sort_id'];
    public  $timestamps=true;
}
