<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    protected $table='product_options';
    protected  $fillable=['product_id','option_id'];
    public  $timestamps=true;

    public function option()
    {
        return $this->hasOne('App\Models\Option','id','option_id');
    }
}
