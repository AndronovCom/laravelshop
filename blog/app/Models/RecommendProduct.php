<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecommendProduct extends Model
{
    //
    protected $table='recommendsProducts';
    protected $fillable=['product_id','product_id_recommend'];
    public  $timestamps=true;

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id_recommend');
    }
}
