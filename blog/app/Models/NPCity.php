<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NPCity extends Model
{
    protected $table='npcity';
    protected $fillable=['name','ref'];

    public $timestamps=false;
}
