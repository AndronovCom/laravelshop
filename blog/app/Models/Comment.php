<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table='comments';
    protected $primaryKey='id';
    protected $fillable=['name','email','msg','product_id','approve'];
    public  $timestamps=true;

    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }
}
