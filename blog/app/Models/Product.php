<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='products';
    protected $primaryKey='id';
    protected $fillable=['name','title','keywords','description','description_full','values','cover','sku','price','price_old','label','isset','urlhash','categories_id','coloures_id'];
    public  $timestamps=true;

    public function recommendProd()
    {
        return $this->hasMany('App\Models\RecommendProduct','product_id');
    }

    public function recommendProds()
    {
        return $this->belongsToMany('App\Models\RecommendProduct','recommendsProducts','product_id','product_id_recommend');
    }

    public function productOptions()
    {
        return $this->belongsToMany('App\Models\Option','product_options','product_id','option_id');
    }

    public function category()
    {
        return $this->hasOne('App\Models\Category','id','categories_id');
    }


    public function comments()
    {
        return $this->hasMany('App\Models\Comment','product_id');
    }

    public function colour()
    {
        return $this->hasOne('App\Models\Colour','id','coloures_id');
    }
}
