<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $table='orders';
    protected $fillable=['client_id', 'delivery_city','delivery_adr', 'delivery_np','delivery_type', 'pay_type', 'code', 'comment', 'ttn'];
    public  $timestamps=true;

    public function client()
    {
        return $this->hasOne('App\Models\Client', 'id', 'client_id');
    }

    public function items(){
        return $this->hasMany('App\Models\OrderItem', 'order_id','id');
    }

    public function files(){
        return $this->hasMany('App\Models\OrderFile', 'order_id','id');
    }

    public function scopeNewOrders($query){
        return $query->where('status', 'new');
    }
}
